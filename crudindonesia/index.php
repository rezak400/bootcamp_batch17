<?php 
	require "model/db.php";
	$lemari= query(	"SELECT * FROM provinsi_tb");
	
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./public/css/bootstrap.min.css">

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h2>Provinsi dan Kabupaten</h2>
            </div>
            <div class="col-6">
                <a class="btn btn-primary" href="./tambah.php" role="button">Add Provinsi</a>
                <button class=" btn btn-success">
                    Add Kabupaten
                </button>
            </div>
        </div>

        <div class="row">
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Provinsi
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <?php foreach($lemari as $buku): ?>
                    
                    <a class="dropdown-item" href="#"><?= $buku["nama"] ?></a>
                <?php endforeach; ?>
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </div>
        </div>
        

        <div class="row justify-content-around">
            <?php foreach($lemari as $buku): ?>
                <div class="col-4 ">
                    <div class="card" style="width: 18rem;">
                        <img src="./public/img/westjava.png" class="card-img-top" alt="...">
                        <div class="card-body">
                        <p class="card-text"><?= $buku["nama"]?></p>
                        <p class="card-text"><?= $buku["diresmikan"]?></p>
                        </div>
                    </div>
                </div>
			<?php endforeach; ?>
        </div>
    </div>
   
    <!-- <div class="row">
        dropdown
    </div>
    -->
    <!-- <div class="row">
       <div class="col">
            <div class="card" style="width: 18rem;">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
            </div>
       </div>
    </div> -->
    <script src="./public/js/jquery-3.3.1.slim.min.js"></script>
    <script src="./public/js/popper.min.js"></script>
    <script src="./public/js/bootstrap.min.js"></script>
    
</body>
</html>