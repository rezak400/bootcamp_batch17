<?php 
	require "model/db.php";
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./public/css/bootstrap.min.css">

</head>
<body>
	<div class="container">
        <div class="row">
            <h2>Tambah Provinsi</h2>
        </div>
        <form action="" method="post">
        <div class="row">

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-default">Nama Provinsi</span>
                </div>
                <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="nama">
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-default">Pulau</span>
                </div>
                <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="pulau">
            </div>

            <h4>Diresmikan </h4>
            <input type="date" name="diresmikan">
            
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile" name="photo">
                <label class="custom-file-label" for="customFile">Foto</label>
            </div>
							
            <button type="submit" name="submit">SUBMIT</button>

            <?php if ( isset($_POST['submit'])) :
								$query= tambahdata($_POST);
							?>
								<?php if ($query > 0) : ?>
									<p>Data berhasil ditambah</p>
								<?php else :
									echo 'error';
									$error=mysqli_error($konek);
									$row=mysqli_affected_rows($konek);
									var_dump($error);
									var_dump($row);

								; endif; ?>

								
							
							<?php endif; ?>
        </div>
        </form>
    </div>
	
</body>
</html>